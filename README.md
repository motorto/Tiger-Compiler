# Trabalho laboratorial de Compiladores (CC3001)

## Compilador para Tiger-0

* Descrição do trabalho em [trabalho.pdf](trabalho.pdf)
* Descrição da linguagem em [tiger0-reference.pdf](tiger0-reference.pdf)

----

Pedro Vasconcelos, 2021

## How to Run

```
cabal update
cabal run
```

Write the code (Ctrl+d) to end input.

Three files will be created:

- ast.txt - correspond to the ast tree
- 3addr.txt - correspond to the 3 address code
- mips.asm - MIPS assembly code

